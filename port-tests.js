
const Promise = require('bluebird')

const async = require('asyncawait').async
const await = require('asyncawait').await

const arduinos = require('./port-api').ports.arduino
const portApi = require('./port-api')

const JSONStream = require('JSONStream')
const Parser = require("stream-json/Parser")
const Streamer = require("stream-json/Streamer")

console.log(arduinos)

async(function() {
  console.log("all")
  await( arduinos.list.then(console.log) )

  console.log("closed")
  await( arduinos.closed.then(console.log) )

  console.log("open")
  await( arduinos.opened.then(console.log) )

  console.log("tests")
  var closed = await( arduinos.closed )
  for (let c of closed)
  {
    let port = new portApi.classes.SerialPort(c)
    console.log(port.id, port.isOpen)

    port.open()
    .then((p)     => console.log(p.id, "open"))
    .then(()      => port.write("hello"))
    .then((data)  => console.log("sent:", data))
    .then(()      => port.close())
    .then((p)     => console.log(p.id,"close"))
    .catch(console.error)
  }
})()
