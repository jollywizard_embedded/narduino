#!/usr/bin/env node

/* Web server dependencies */
const express = require('express')
const bodyParser = require('body-parser')

/* Arduino dependencies */
const serial = require('serialport')

/* Web server config */
const app = express()

const DEFAULT_PORT = 8080
const port = getPort()
function getPort() { return process.env.PORT || DEFAULT_PORT }

app.set('json spaces', 2)

/* Configure Routes */
const serial_router = express.Router();

serial_router.get('/list/', function(req,res) {
	serial.list(function(err, ports) {
    res.json(ports)
  })
})

serial_router.get('/list-arduino/', function(req, res) {
	serial.list(function(err, ports) {
    res.json(
        ports.filter(isArduinoPort)
    )
  })
})

var currentPort;

serial_router.get('/open/', function(req, res) {
  comName = JSON.parse(req.body).comName
  currentPort = new SerialPort(comName)
  currentPort.open(function(err) {
    res.json({request:"open",comName: comName, err: err})
  })
})

app.use('/', express.static('webapp'))

app.use('/serial', serial_router)


function isArduinoPort(port) {
  if (port.comName.includes("ACM"))
    return true

  return false;
}

app.listen(port)
console.log("active on port:", port)
