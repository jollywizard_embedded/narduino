
/* requires 5.x, i.e. explicit*/
const serial = require('serialport')
const Promise = require('bluebird')

const async = require('asyncawait').async
const await = require('asyncawait').await

const openPorts = []

function isOpen(port) {
  if (!port) return false
  if (typeof port === 'string')
    return openPorts.includes(port)
  else if (!!port.comName) {
    return isOpen(port.comName)
  }
  return false
}

function isClosed(port) {return !isOpen(port)}

function filterOpened(ports) {return ports.filter(isOpen)}

function filterClosed(ports) {return ports.filter(isClosed)}

class SerialPorts {

  get list() {
    return Promise.fromCallback(serial.list)
  }

  get opened() {
    return this.list.then(filterOpened)
  }

  get closed() {
    return this.list.then(filterClosed)
  }

} // SerialPorts

class ArduinoPorts extends SerialPorts {

  get list() {
    return super.list.then(ArduinoPorts.filterArduinoPorts)
  }

  static filterArduinoPorts(ports) {
    return ports.filter(ArduinoPorts.isArduinoPort)
  }

  static isArduinoPort(port)
  {
    return (port.comName.includes("ACM"))
  }

} // ArduinoPorts

function handlePortError(port, err) {
  port._errors.push(err)
  throw err
}

function handleOpen(port) {
  return function(err) {
    if (err)
    {
      handlePortError(port, err)
    }
    else
    {
      port._startTime = new Date()
      port._isOpen = true
      openPorts.push()
    }
  }
}

function basicPromiseCallback(value) {
  return (resolve, reject) => {
    return (err, params) => {
        if (err) reject(err)
        else resolve(!!params ? params: value)
    }
  }
}

class SerialPort {

  constructor(portId, opts = {})
  {
    if (typeof(portId) === 'object')
      portId = portId.comName

    if (!opts.autoOpen)
      opts.autoOpen = false

    this._promiseCallback = basicPromiseCallback(this)
    this._portId = portId
    this._isOpen = false
    this._errors = []
    this._raw_port = new serial(portId, opts, handleOpen(this))
  }

  get id()
  {
    return this._portId;
  }

  get lastError()
  {
    return _errors.length ? _errors[_errors.length - 1] : null
  }

  get port()
  {
    return this._raw_port
  }

  get isOpen()
  {
    return this._isOpen
  }

  open()
  {
    return new Promise( (resolve, reject) => {
      this.port.open(this._promiseCallback(resolve, reject))
    })
  }

  close()
  {
    return new Promise( (resolve, reject) => {
      this.port.close(this._promiseCallback(resolve, reject))
    })
  }

  probe()
  {
    return this.open()
    .then(this.close.bind(this))
  }

  write(data, drainImmediately = true)
  {
    let port = this.port
    ,   drain = this.drain.bind(this)

    return new Promise( (resolve, reject) => {
      port.write(data, function(err) {
        if (err)
        {
          return reject(err)
        }
        else if (drainImmediately)
        {
          drain().then(() => resolve(data))
        }
        else resolve(data)
      })
    })
  }

  drain()
  {
    return new Promise( (resolve, reject) => {
      this.port.drain(this._promiseCallback(resolve, reject))
    })
  }

} // SerialPort

module.exports = {
    ports : {
        serial : new SerialPorts()
    ,   arduino : new ArduinoPorts()
    }
  , classes : {
        SerialPorts : SerialPorts
    ,   ArduinoPorts : ArduinoPorts
    ,   SerialPort : SerialPort
  }
}
